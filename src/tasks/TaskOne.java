package tasks;

import java.util.ArrayList;
import java.util.List;

public class TaskOne {
    public static String[] createUniqueArray(String[] array) {
        List<String> uniqueList = new ArrayList<>();
        for(int i = 0; i < array.length; i++) {
            if(!uniqueList.contains(array[i])) {
                uniqueList.add(array[i]);
            }
        }
        String[] newArray = new String[uniqueList.size()];
        newArray = (String[]) uniqueList.toArray(newArray);
        return newArray;
    }

    public static Integer[] createUniqueArray(Integer[] array) {
        List<Integer> uniqueList = new ArrayList<>();
        for(int i = 0; i < array.length; i++) {
            if(!uniqueList.contains(array[i])) {
                uniqueList.add(array[i]);
            }
        }
        Integer[] newArray = new Integer[uniqueList.size()];
        newArray = uniqueList.toArray(newArray);
        return newArray;
    }
}
