package tasks;

import java.util.Arrays;

import static java.lang.Integer.parseInt;

public class TaskFour {
    public static int[] timeSum(String[] timeArray) {
        if (timeArray.length == 0) {
            return new int[]{0, 0 ,0};
        }
        String[][] splitTime = new String[timeArray.length][3];
        int[] timeTotal = new int[3];
        for (int i = 0; i < timeArray.length; i++) {
            splitTime[i] = timeArray[i].split(":");
            for (int j = 0; j < splitTime[i].length; j++) {
                timeTotal[j] += parseInt(splitTime[i][j]);
            }
        }

        if (timeTotal[2] > 60) {
            timeTotal[1] += Math.floor(timeTotal[2] / 60);
            timeTotal[2] = timeTotal[2] % 60;
        }

        if (timeTotal[1] > 60) {
            timeTotal[0] += Math.floor(timeTotal[1] / 60);
            timeTotal[1] = timeTotal[1] % 60;
        }

        return timeTotal;
    }
}
