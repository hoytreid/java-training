package tasks;

import java.util.ArrayList;
import java.util.List;

public class TaskSeven {
    public static String[] removeAlphabetChars (String[] stringArray) {
        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < stringArray.length; i++) {
            if (!stringArray[i].matches("[a-zA-Z]+") && !stringList.contains(stringArray[i])) {
                stringList.add(stringArray[i].trim());
            }
        }

        String[] numericArray = new String[stringList.size()];

        return stringList.toArray(numericArray);
    }
}
