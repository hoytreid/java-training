package tasks;

public class TaskThree {
    public static boolean validateEmail(String email) {
        boolean isValid = false;
        if (email.contains("@") && email.contains(".") && email.indexOf('@') < email.lastIndexOf('.') && email.indexOf('@') != 0) {
            isValid = true;
        }
        return isValid;
    }
}
