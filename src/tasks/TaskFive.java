package tasks;

public class TaskFive {
    public static boolean validatePhonenumber(String phoneNumber) {
        String areaCode = phoneNumber.substring(1, 4);
        String longNumber = String.join("", phoneNumber.substring(6).split("-"));

        if (areaCode.matches("[0-9]+") &&
                longNumber.matches("[0-9]+") &&
                longNumber.length() == 7 &&
                phoneNumber.charAt(0) == '(' &&
                phoneNumber.charAt(4) == ')' &&
                phoneNumber.charAt(5) == ' ' &&
                phoneNumber.charAt(9) == '-') {
            return true;
        }

         return false;
    }
}
