package tasks;

public class TaskSix {
    public static int countBinaryOnes(int number) {
       char[] binary = Integer.toBinaryString(number).toCharArray();
       int binaryOnes = 0;

       for (char bit : binary) {
           binaryOnes += Character.getNumericValue(bit);
       }
       return binaryOnes;
    }
}
